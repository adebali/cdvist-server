
import subprocess
import sys



# oat: Ogun Adebali's Tools


def removeEmptyElements(theList):
	newList = []
	for e in theList:
		if e.strip():
			newList.append(e)
	return newList

def manInOut(letters):
	ll = letters.split()
	mydict = {}
	for letter in letters:
		if '-'+letter in sys.argv:
			mydict[letter] = sys.argv[sys.argv.index('-'+letter)+1]
		else:
			print("Mandatory option -" + letter + " not given, exiting...")
			sys.exit()
	return mydict


def optopt(letters):
	ll = letters.split(',')
	mydict = {}
	for letter in ll:
		if '-'+letter in sys.argv:
			mydict[letter] = sys.argv[sys.argv.index('-'+letter)+1]
		else:
			mydict[letter] = ""
			print("Optional optrion -" + letter + " not given, I am using the default or ignoring this option")
	return mydict
			
	

def flag(theflag):
	if '--'+theflag in sys.argv:
		return 1
	else:
		return 0

class AutoVivification(dict):
    """Implementation of perl's autovivification feature."""
    def __getitem__(self, item):
        try:
            return dict.__getitem__(self, item)
        except KeyError:
            value = self[item] = type(self)()
            return value


def systemCall(command):
	#l = command.split(' ')
	#nl = []
	#for e in l:
	#	if e:
	#		nl.append(e)
	return subprocess.check_output(command, shell=True).decode("utf-8")
	#proc = subprocess.Popen(nl, stdout=subprocess.PIPE, shell=True)
	#(out, err) = proc.communicate()
	#return out.decode("utf-8")




def fastareader(datafile, just_name = 'no'):
        data = open(datafile,'r')
        seq_dic = {}
        list_order = []
        for line in data:
                line = line.replace('\r','')
                if line[0] == '>':
                        if just_name == 'Yes':
                                name = line.split('/')[0][1:]
                        else:
                                name = line[1:-1]
                        list_order.append(name)
                        seq_dic[name] = ''
                else:
                        while line.find('\n') != -1:
                                line = line[0:line.find('\n')] + line[line.find('\n')+2:]
                        seq_dic[name]= seq_dic[name] + line
#       dataout = ''
#       for k, v in seq_dic.iteritems():
#               dataout = dataout + '>' + k + '\n' + v + '\n'

        data.close()
        return seq_dic, list_order

def addToSet(l,e):
	if not e in l:
		l.append(e)
	return l


import sys
import os
from urllib.request import urlopen
from pymongo import MongoClient
import xmltodict

def gi2geneIDandTaxid(gi):
	url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id=" + str(gi) + "&retmode=xml"
	xmlFile = urlopen(url).read()
	myDict = xmltodict.parse(xmlFile)
	taxid = "NA"
	geneid = "NA"
	GBFeatures = myDict['GBSet']['GBSeq']['GBSeq_feature-table']['GBFeature']
	


	for GBFeature in GBFeatures:
		if GBFeature['GBFeature_key'] == "source":
			GBFeature_quals = GBFeature['GBFeature_quals']['GBQualifier']
			for qual in GBFeature_quals:
				if 'GBQualifier_value' in qual.keys():
					if 'taxon' in qual['GBQualifier_value']:
						taxid = int(qual['GBQualifier_value'].split(':')[1])
						break
				
		elif  GBFeature['GBFeature_key'] == "CDS":
			GBFeature_quals = GBFeature['GBFeature_quals']['GBQualifier']
			for qual in GBFeature_quals:
				if 'GBQualifier_value' in qual.keys():
					if 'GeneID' in qual['GBQualifier_value']:
						geneid = int(qual['GBQualifier_value'].split(':')[1])
						break
					elif 'WormBase' in qual['GBQualifier_value']:
						geneid = qual['GBQualifier_value'].split(':')[1]

		if geneid != "NA" and taxid !="NA":
			break
	if geneid != "NA" and taxid !="NA":
		return taxid,geneid
	else:
		print("Complain!!")
		print(gi)
		sys.exit()
def gi2seq(gi):
	url = "http://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id=" + str(gi) + "&rettype=fasta"
	fastaText = str(urlopen(url).read().decode("utf-8"))
	fl = fastaText.split('\n')
	seq = ""
	for i in range(1,len(fl)):
		seq += fl[i]
	return seq
