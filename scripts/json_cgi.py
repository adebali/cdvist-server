#!/usr/bin/python

# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time
from glob import glob

form = cgi.FieldStorage()
arguments = form
message = form.getvalue('Submit')
if str(message) == "None":
	message = "Status Check"


def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return

myhtmlID = str(arguments['htmlID']).split(',')[1].split(')')[0].strip().replace('\'','')

if message == "Status Check":
	code = "python table2domfigure_nv1.py -i ../FASTA/FASTA" + myhtmlID + ".fa -o ../codost_out/" + myhtmlID + ".html -newton " + myhtmlID
	os.system(code)

elif message == "Kill Jobs":
	code = "python table2domfigure_nv1.py --kill -i ../FASTA/FASTA" + myhtmlID + ".fa -o ../codost_out/" + myhtmlID + ".html -newton " + myhtmlID
	os.system(code)
	

	
url = "../codost_out/" + str(int(myhtmlID)) + ".html"
meta = '<meta http-equiv="refresh" content="URL=' + url + '">'

print "Content-type:text/html\r\n\r\n"
print '''
	
        <html>
       	<head>
        <title>CDvist</title>'''
print meta
print '''
        </head>
        <body>
        Your work is being processed. Please be patient
							        
'''								        
							        
print message

print '''</body></html>'''
