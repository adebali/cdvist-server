import os
import sys
import re

inputfile = sys.argv[1]
maxSeqNum = 500

def skimInput(inputfile)
	numberOfGT = int(os.popen("grep -c '>' " + inputfile))
	if numberOfGT > maxSeqNum:
		status = 1
	else:
		status = 0
	
	if status == 0:
		return "Incorrect FASTA format or sequence number exceeds the maximum limit of " + str(maxSeqNum)
	elif status == 1:
		return "PASS"

def fastaReaderCorrecter(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + re.sub(r'x|X|b|B|j|J|o|O|x|X|z|Z|\d|\W,','',line)
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return dataout


def checkFASTAformat(inputfile):
	dataout = ''
	try:
		dataout = fastaReaderCorrecter(inputfile)
	except:
		return "Incorrect FASTA format"
	
	if len(l) > maxSeqNum:
		return "Sequence number exceeds the maximum limit of " + str(maxSeqNum)
	return "PASS",dataout

def displayError(status):
	htmltext = '''
		Content-type:text/html\r\n\r\n
		<html>	<head><title>CDvist</title></head>
			<body>
		'''
	htmltext += status
	htmltext += '</body></html>'
	print htmltext

status1 = skimInput(inputfile)
if status1 == "PASS":
	status2, dataout == checkFASTAformat(inputfile)
	if status2 == "PASS":
		outfile.write(dataout)
		outfile.close()
	else:
		displayError(status2)
else:
	displayError(status1)


