#!/usr/bin/python

# Import modules for CGI handling 
import cgi, cgitb 
import os
import datetime
import sys
import subprocess
import time
from glob import glob
import re
import json

print "Content-type: text/html\n\n" 

maxSeqNum = 2000

form = cgi.FieldStorage() 
# Get data from fields
fileitem = form['BROWSED']
sequence = form.getvalue('SEQUENCE')

def displayError(status):
        htmltext = '''
                <html>  <head><title>CDvist Error</title></head>
                        <link rel="stylesheet" type="text/css" href="http://web.utk.edu/~oadebali/codost_style.css">
			<body>
			<p id="Ztitle">CDvist Error!</p>
			<p id="Zh2">--> 
                '''
        htmltext += status
        htmltext += '</p></body></html>'
        print htmltext
	sys.exit()

def skimInput(inputfile):
	code = "grep -c '>' " + inputfile
	#print code
        numberOfGT = int(os.popen(code).read())
        if numberOfGT > maxSeqNum:
                status = 0
        else:
                status = 1 

        if status == 0:
                return "Error 662 Incorrect FASTA format or sequence number exceeds the maximum limit of " + str(maxSeqNum)
        elif status == 1:
                return "PASS"

def fastaReaderCorrecter(datafile, just_name = 'no'):
        data = open(datafile,'r')
        seq_dic = {}
        list_order = []
	s = ''
	max_seq_length = 1
	count = 0
        for line in data:
                line = line.replace('\r','')
                if line[0] == '>':
			count += 1
			name = line[1:-1]
			name = re.sub(r'\#|\?|\!|\@|\$|\%|\^|\&|\*|\(|\)|\=','-',name)
			#if any(name == s for s in list_order) or name == '' or any(name[:40] == s[:40] for s in list_order):
			if any(name == s for s in list_order) or name == '':
				#return list_order, dataout,"Duplicated or void sequence header"
				#displayError("Duplicated or void sequence header")
				#name += str(count)
				name = name + str(count)
			list_order.append(name)
                        seq_dic[name] = ''
                else:
                        while line.find('\n') != -1:
                                line = line[0:line.find('\n')] + line[line.find('\n')+2:]
                        seq_dic[name]= seq_dic[name] + re.sub(r'b|B|j|J|o|O|z|Z|\#|\?|\!|\@|\$|\%|\^|\&|\*|\(|\)|\=,','',line)
			max_seq_length = max(max_seq_length,len(seq_dic[name]))
        dataout = ''
        
	

	for header in list_order:
                dataout = dataout + '>' + header + '\n' + seq_dic[header] + '\n'

        data.close()
	s = "PASS"
        return list_order, dataout, s, max_seq_length


def checkFASTAformat(inputfile):
        dataout = ''
        try:
                l, dataout, s, max_seq_length = fastaReaderCorrecter(inputfile)
		if s != "PASS":
			displayError(s)
        except:
                displayError(" incorrect FASTA format")

        if len(l) > maxSeqNum:
                displayError("Sequence number exceeds the maximum limit of " + str(maxSeqNum))
        
	return dataout, max_seq_length



def qualityControl(inputfile,outputfile):
	outfile = open(outputfile,'w')
	status1 = skimInput(inputfile)
	if status1 == "PASS":
		dataout, max_seq_length = checkFASTAformat(inputfile)
		
		#print outputfile
		outfile.write(dataout)
		outfile.close()
		return max_seq_length
	else:
        	displayError(status1)


def removefile(file):
	if os.path.isfile(file):
		code='rm '+file
		os.system(code)
		return
	else:
		return


def thereisnoinput():
	print "Content-type:text/html\r\n\r\n"
	print "<html>"
	print "<head>"
	print "<title>CDvist</title>"
	print "</head>"
	print "<body>"
	print "There is no input!"
	print "</body>"
	print "</html>"


def intheprocesspage(outhtml):
	filename='../codost_out/'+outhtml
	#filename='process.html'
	web=open(filename,'w')
	text=''' 
	<html>
	<head>
	<title>CDvist</title>
	<meta http-equiv="refresh" content="5" >
	</head>
	<body>
	Your work is being processed. Please be patient
	</body>
	</html>
	'''
	web.write(text)
	web.close()
	return 1


def timeDiff(t):
	def t2sec(timestring):
		return int(timestring[4:6])*30*24*24*60 + int(timestring[6:8])*24*24*60 + int(timestring[8:10])*24*60 + int(timestring[10:12])*60 + int(timestring[12:14])
	currenttime = t2sec(str(datetime.datetime.now()).replace(" ","").replace("-","").replace(":","").replace(".",""))
	oldtime = t2sec(t)
	diff = currenttime - oldtime
	return diff
	

	
def generateparametersline(databasetext,flags):
	parametersline=' --print -cpu 1 --nodraw -d '+databasetext+' '+flags
	return parametersline
	

def printFirstMoment(link):

	print'''
	<html>
		<head>
			<title>CDvist</title>
			<meta http-equiv="refresh" content="1;url='''+link+'''" />	
                        <link rel="stylesheet" type="text/css" href="http://web.utk.edu/~oadebali/codost_style.css">
			</head>
		
		<body onload=newDoc()>
			<p id="Ztitle">CDvist</p>
			<p id="Zh2">Your input sequences are valid</p>
			<p id="Zh2">Directing to a self-refreshing status page in a moment.</p>
			<p id="Zh2"><a href='''+link+''' target=\"_blank\">'''+link+'''</a></p>

		</body>
	</html>'''


# Generate a UNIQUE ID
mytime=datetime.datetime.now()
t=str(mytime).replace(" ","").replace("-","").replace(":","").replace(".","")
outhtml=t+".html"

link= "../codost_out/CDvistJ.html?f=" + t + "/" + t + ".json"

# Create instance of FieldStorage 


Ali = form.getvalue('Ali')
if Ali == "Yes":
	ALI = 1
else:
	ALI = 0


# Define the locations and names for inputs
tempfilename = '../TEMP/TEMP'+t+'.fa'
fastafilename='../requests/FASTA'+t+'.fa'
ALIfilename='../codost_out/'+t+'/aligned.json'
ALIjson='../codost_out/'+t+'/aligned.json'
permanentfastafilename='../FASTA/FASTA'+t+'.fa'
paramfilename = parametersfilename='../requests/PARAM'+t+'.txt'
permanentparamfilename = '../PARAM/PARAM'+t+'.txt'

os.system("mkdir ../codost_out/" + t)

# Save the fasta file of user input
textbox=0
cpfasta = "cp " + fastafilename + " " + permanentfastafilename
cpparam = "cp " + paramfilename + " " + permanentparamfilename
cpAli = "python fa2json_server.py " + permanentfastafilename + " " + ALIfilename


if not fileitem.filename and sequence == '':
	displayError("No input")

if fileitem.filename:
	#open(fastafilename, 'wb').write(fileitem.file.read())
	open(tempfilename, 'wb').write(fileitem.file.read())
	max_seq_length = qualityControl(tempfilename,fastafilename)
	os.system(cpfasta)



elif sequence:
	textbox=1
	fas=open(tempfilename,'w')
	fas.write(sequence)
	fas.close()
	max_seq_length = qualityControl(tempfilename,fastafilename)
	os.system(cpfasta)


else:
	displayError("No input")


os.system(cpAli)

def removeGaps(filename):
	text = ""
	filein = open(filename,'r')
	for line in filein:
		if line.startswith(">"):
			text += line
		else:
			text += line.replace("-","").replace("X","").replace("x","")
	filein.close()
	out = open(filename,'w')
	out.write(text)
	out.close()

removeGaps(fastafilename)

#print "Content-type:text/html \r\nContent-Length: 348\r\n\r\n"
printFirstMoment(link)




flags=' -newton '+t+' '


if max_seq_length <= 1000:
	joblength = "short"
elif max_seq_length <=3000:
	joblength = "medium"
else:
	joblength = "long"

flags += '-joblength ' + joblength + ' '

hhblitsdb=form.getvalue("bdatabase")
if hhblitsdb != 'None':
	hhblitsprobability=form.getvalue("blitsprob")
	flags=flags+' -b '+hhblitsdb+'_'+hhblitsprobability+' '

# Work on the parameters
databasetext=''
runpfamscan=form.getvalue("pfamscan")
if runpfamscan:
	runhmmer=1
else:
	runhmmer=0
	runpfamscan="False"
	
rpsblast = form.getvalue("rpsblast")
if rpsblast:
	runrpsblast = 1	
	rpsdatabase = "Cdd"
	rpsgaplength = 30
	rps_evalue = float(form.getvalue("rps_evalue"))
else:
	runrpsblast = 0

if runrpsblast:
	flags += ' -rpsblast ' + rpsdatabase + '_' + str(rps_evalue) + '_' + str(rpsgaplength) + ' '


rundomainsplit=form.getvalue("domainsplit")
if rundomainsplit:
	domainsplit=1
	domainsplitpercentage=float(form.getvalue("domainsplitpercentage"))
	flags=flags+' -splitdomain '+str(domainsplitpercentage)+' '
else:
	domainsplit=0
	domainsplitpercentage=100.0


tmprediction=form.getvalue("TM")
if tmprediction == "tmhmm":
	TMmethod="tmhmm"
	runTM=1
elif tmprediction == "phobius":
	TMmethod="phobius"
	runTM=1
else:
	TMmethod="tmhmm"
	runTM=1

if runhmmer==0:
	flags=flags+' --nohmmer'
	
if runTM==0:
	flags=flags+' --noTM'
elif runTM==1:
	flags=flags+' -tm '+TMmethod

for i in range(1,6):
	db="database"+str(i)
	db=form.getvalue(db)
	prob="prob"+str(i)
	prob=form.getvalue(prob)
	gap="gap"+str(i)
	gap=form.getvalue(gap)
	databasetext=databasetext+str(db)+'_'+str(prob)+'_'+str(gap)+'AND'

databasetext=databasetext[:-3]

parameters=generateparametersline(databasetext,flags)

#Write parameters file
pfile=open(parametersfilename,'w')
pfile.write(parameters)
pfile.close()
os.system(cpparam)
def waitPage(t):
	# Open the Pre-Results Page

	filepath="../codost_out/"+t+".html"
	#print filepath
	preresultf=open(filepath,'w')
	preresultf.write('''<html>
		<head>
			<title>CDvist-Processing</title>
			<LINK REL="SHORTCUT ICON"  HREF="http://web.utk.edu/~oadebali/waiting.ico">
			<meta http-equiv="content-type" content="text/html; charset=UTF-8">
			<meta http-equiv="refresh" content="60">

		</head>
	
		<body>
			<h2>CDvist</h2>
				<p>Your results will be here when ready!</p>
				<p>This page will be refreshed every minute.</p>
				<p></p>
				<p>Workflow example:</p>
				<img src="http://web.utk.edu/~oadebali/oksuzlar.gif" width="500" height="300"/>
		</body>
	</html>''')
	preresultf.close()

#waitPage(t)
#intheprocesspage(t)

code1 = 'ssh newton "bash /home/oadebali/codost/scripts/requestcheckJ.sh" >requestcheckJ.log'
sshfailed = os.system(code1)




def fastareader(datafile, just_name = 'no'):
        data = open(datafile,'r')
        seq_dic = {}
        list_order = []
        for line in data:
                line = line.replace('\r','')
                if line[0] == '>':
                        if just_name == 'Yes':
                                name = line.split('/')[0][1:]
                        else:
                                name = line[1:-1]
                        list_order.append(name)
                        seq_dic[name] = ''
                else:
                        while line.find('\n') != -1:
                                line = line[0:line.find('\n')] + line[line.find('\n')+2:]
                        seq_dic[name]= seq_dic[name] + line
        dataout = ''
        for k, v in seq_dic.iteritems():
                dataout = dataout + '>' + k + '\n' + v + '\n'

        data.close()
        return seq_dic, list_order


mydict = {}
d,l = fastareader(permanentfastafilename)
out = open(ALIjson,"w")

mydict["entries"] = []

i = 0
for e in l:
	i += 1
	order = i
	name = e
	s = d[e]
	mydict["entries"].append({'header':name,'num':i,'fullsequence':s, "status":"queued"})

myjson = json.dumps(mydict, sort_keys=True, indent= 1)
out.write(myjson)


#print sshfailed
#ID = t
#code2 = "python json2domfigure.py --empty -i ../FASTA/FASTA" + ID + ".fa -o ../codost_out/" + ID + ".html -newton " + ID
