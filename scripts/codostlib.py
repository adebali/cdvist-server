#!/usr/bin/env python
#####by###OGUN ADEBALI###########
####UNIVERSITY OF TENNESSEE######
#Last Update: 28 November 2013
#################################
import os
import sys
import datetime
import re
import json
import codostlib as co

def jsondata2features(data):
#	data = json.loads(jsondata)
        header = data['header']
        fullsequence = data['fullsequence']
        TMinfo = data['TM']
        segInfo = data['seg']
        coilsInfo = data['coils']
        #tag=co.removesymbols(header)[:50]
        tag = header[:50]
        seqlength = len(fullsequence)
        features='\''+tag+'\''+','+'\''+str(fullsequence)+'\''+','+'\''+str(TMinfo)+'\''+','+'\''+str(segInfo)+'\''+','+'\''+str(coilsInfo)+'\''+','
        domains=''
        intervals=''
        scores=''
        databases=''
        coverages=''
        sequences=''
        alignments=''

        for segment in data['segments']:
                domains += segment['domain']+';'
                intervals += segment['interval']+';'
                scores += segment['score']+';'
                databases += segment['database']+';'
                sequences += segment['sequence']+';'
                alignments += segment['alignment']+';'
                coverages += segment['coverage'].replace("|","_")+';'

        features=features+'\''+domains+'\''+','+'\''+intervals+'\''+','+'\''+scores+'\''+','+'\''+databases+'\''+','+'\''+coverages+'\''+','+'\''+sequences+'\''+','+'\''+alignments+'\''
        return features
	


def json2features(json_in):
        jsondata = open(json_in).read()
        data = json.loads(jsondata)
        return jsondata2features(data)



def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order




def removesymbols(text):
	if re.match('^[0-9]',text):
		text = "N" + text
	return text.replace(' ','_').replace('[','_').replace(']','_').replace('{','_').replace('}','_').replace(':','_').replace('\n','').replace(';','_').replace('|','_').replace(',','_').replace('(','_').replace(')','_').replace('/','_').replace('\\','_').replace('\'','_').replace(".","").replace("'","_").replace("\"","_")


def fastareader(datafile, just_name = 'no'):
	data = open(datafile,'r')
	seq_dic = {}
	list_order = []
	for line in data:
		line = line.replace('\r','')
		if line[0] == '>':
			if just_name == 'Yes':
				name = line.split('/')[0][1:]
			else:
				name = line[1:-1]
			list_order.append(name)
			seq_dic[name] = ''
		else:
			while line.find('\n') != -1:
				line = line[0:line.find('\n')] + line[line.find('\n')+2:]
			seq_dic[name]= seq_dic[name] + line
	dataout = ''
	for k, v in seq_dic.iteritems():
	 	dataout = dataout + '>' + k + '\n' + v + '\n'
	
	data.close()
	return seq_dic, list_order
	
def deneme():
	return "poopo"
		
