#!/usr/bin/env python


import os
import sys
import datetime
import re
import codostlib as co
import subprocess
import json
from glob import glob
from math import floor
from math import fabs

timestart=datetime.datetime.now()

arg=[]
arg = sys.argv
help = "Help Info\n__________\nThis script takes fasta file as an input, reads headers, finds the tables, and uses the tables as inputs"
help = help+"\n\nusage: table2domfigure.py -i yourfastafile.fa \n\noptional: \n-o output.html"
help = help+"\n\nPS: In your fasta file first 50 characters of each header must be unique!\n"


def getJobID(ID):
	filein = open("STATUS"+ID,"r")
	filein.readline()
	filein.readline()
	jobID = int(filein.readline()[0:8])
	return str(jobID)

def getParameters(ID):
	paramfile = '../PARAM/PARAM' + str(ID) + '.txt'
	return open(paramfile,'r').readline()

def checkPossibleError(JobID,proteinNumber):
	qstatinfo = subprocess.check_output(['ssh'],['newton'],['\'qstat\''],['2>ssherrors.txt'])
	a= 'CODOST_20140517173857289529.e762207.33'


if '-h' in sys.argv:
        print help+"\n"
        sys.exit()

if '-i' in sys.argv:
        inputfile=sys.argv[sys.argv.index('-i')+1]
        seq_dic, seq_list = co.fastareader(inputfile)
else:
        print "No input\n"
        sys.exit()
#cprintout = open("cprint.txt","w")
def cprint(thetexttobeprinted):
        cprintout.write(str(thetexttobeprinted))
        return

def ifComplete(ID):
	TableCount = len(glob('../codost_out/' + ID + "*.json"))
	#cprint(TableCount)
	TotalProteinCount = len(seq_list)
	#cprint(TotalProteinCount)
	if TableCount >= TotalProteinCount:
		return 1
	else:
		return 0




if '--empty' in sys.argv:
	empty = 1
	jobId = ''
else:
	empty = 0

if '--kill' in sys.argv:
	kill = 1
else:
	kill = 0

if '-o' in sys.argv:
	outputfile =sys.argv[sys.argv.index('-o')+1]
else:
	outputfile = inputfile[:(inputfile.rindex('.')-len(inputfile))]+'.html'


if '--print' in sys.argv:
	printsign=1
else:
	printsign=0		

if '-newton' in sys.argv:
	newton=1
	ID=sys.argv[sys.argv.index('-newton')+1]
	complete = ifComplete(ID)
	if empty:
		requestNotTaken = [1]
	else:
		requestNotTaken = glob("../requests/FASTA" + ID + ".fa")
	
		if requestNotTaken==[]:
			requestTaken = 1
			os.system("bash getSTATUS.sh " +ID)
			STATUSFILE = "STATUS" + ID
			jobID= getJobID(ID)
		else:
			requestTaken = 0
else:
	newton=0
	
if '-param' in sys.argv:
	param=1
	parameterfile=sys.argv[sys.argv.index('-param')+1]
else:
	param=0

if kill==1:
	#jobID = getJobID(ID)
	code = "ssh newton qdel " + jobID + " >qdel.log"
	os.system(code)


def letter2status(letter):
	if "q" in letter:
		return "queued"
	elif "r" in letter or "t" in letter:
		return "running"
	elif "E" in letter:
		return "error"
	else:
		return "error"

def getStatusDictionary():
	task_status_Dict={}
	statusfile = open(STATUSFILE,"r")
	statusfile.readline()
	statusfile.readline()
	for line in statusfile:
		ll = line.split(' ')
	#	print ll[-1]
		#try:
			
			#taskID = int(ll[-1])
		#except:
		#	st = int(ll[-1].split('-')[0])
		#	end = int(ll[-1].split('-')[1].split(':')[0])
		#for m in range(st,end):
		#	task	
		llll = []
		lll = line.split(' ')
		for ele in lll:
			ele=ele.strip()
			if ele:
				llll.append(ele)
		task_status_Dict[str(llll[-1])] = llll[4]

	return task_status_Dict


def timeDiff(t):
        def t2sec(timestring):
                return fabs(int(timestring[4:6])*30*24*24*60 + int(timestring[6:8])*24*24*60 + int(timestring[8:10])*24*60 + int(timestring[10:12])*60 + int(timestring[12:14]))
        currenttime = t2sec(str(datetime.datetime.now()).replace(" ","").replace("-","").replace(":","").replace(".",""))
        oldtime = t2sec(t)
        diff = currenttime - oldtime
        return fabs(diff)


def getStatus(proteinnumber,task_status_Dict):
	try:
		status = task_status_Dict[str(proteinnumber)]
	except:
		status = "queued"

	return letter2status(status)

def mergeJsonFiles(ID):
	outfilename = "../codost_out/" + ID + ".json"
	mylist = glob("../codost_out/" + ID + "?*.json" )
	os.system('echo "[" >'+outfilename)
	count = 0
	leng = len(mylist)
	for file in mylist:
		count += 1
		os.system('echo "\n"'+' >>'+outfilename)
		os.system('cat '+ file +' >>'+outfilename)
		if count != leng:
			os.system('echo "," >>'+outfilename)
	os.system('echo "\n]" >>'+outfilename)


def table2features(table_in):
	table=open(table_in,'r')
	lines=table.readlines()
	
	header=lines[0].replace('\n','')
	fullsequence=lines[1].replace('\n','')
	TMinfo=lines[2].replace('\n','')
	del lines[0]
	del lines[0]
	del lines[0]
	tag=co.removesymbols(header)[:50]
	
	seqlength=len(fullsequence)
	
	features='\''+tag+'\''+','+'\''+str(fullsequence)+'\''+','+'\''+str(TMinfo)+'\''+','
	domains=''
	intervals=''
	scores=''
	databases=''
	coverages=''
	sequences=''
	alignments=''
	
	
	for line in lines:
		if line:
			if not 'XXXX' in line.split('\t')[0]: 
				
				domains=domains+line.split('\t')[0]+';'
				intervals=intervals+line.split('\t')[1]+';'
				scores=scores+line.split('\t')[2]+';'
				databases=databases+line.split('\t')[3]+';'
				sequences=sequences+line.split('\t')[5].replace("\n","")+';'
				alignments=alignments+line.split('\t')[6].replace("\n","").replace(" ","_")+';'
				
				
				cov=line.split('\t')[4].split('|')[0].strip()+'_'+line.split('\t')[4].split('|')[1].strip()+'_'+line.split('\t')[4].split('|')[2].strip()
				
				#cprint (cov+'\n')
				coverages=coverages+cov+';'
			
		
	features=features+'\''+domains+'\''+','+'\''+intervals+'\''+','+'\''+scores+'\''+','+'\''+databases+'\''+','+'\''+coverages+'\''+','+'\''+sequences+'\''+','+'\''+alignments+'\''
	#cprint (coverages)
	#print features
	return features	

def json2features(json_in):
	jsondata = open(json_in).read()
	data = json.loads(jsondata)
	header = data['header']
	fullsequence = data['fullsequence']
	TMinfo = data['TM']
	segInfo = data['seg']
	coilsInfo = data['coils']
	#tag=co.removesymbols(header)[:50]
	tag = header[:50]
	seqlength = len(fullsequence)
	features='\''+tag+'\''+','+'\''+str(fullsequence)+'\''+','+'\''+str(TMinfo)+'\''+','+'\''+str(segInfo)+'\''+','+'\''+str(coilsInfo)+'\''+','
	domains=''
	intervals=''
	scores=''
	databases=''
	coverages=''
	sequences=''
	alignments=''
	
	for segment in data['segments']:
		domains += segment['domain']+';'
		intervals += segment['interval']+';'
		scores += segment['score']+';'
		databases += segment['database']+';'
		sequences += segment['sequence']+';'
		alignments += segment['alignment']+';'
		coverages += segment['coverage'].replace("|","_")+';'
	
	features=features+'\''+domains+'\''+','+'\''+intervals+'\''+','+'\''+scores+'\''+','+'\''+databases+'\''+','+'\''+coverages+'\''+','+'\''+sequences+'\''+','+'\''+alignments+'\''
	return features
	



def htmlprint_initialize():
	return '''
<html>
	<head>
		<LINK REL="SHORTCUT ICON"  HREF="http://web.utk.edu/~oadebali/CoDAT.ico">
		<title>Domain Architecures</title>
		<meta http-equiv="Content-type" content="text/html;charset=UTF-8">
		<link rel="stylesheet" type="text/css" href="http://web.utk.edu/~oadebali/codost_style.css">
		<SCRIPT LANGUAGE="JavaScript">
		</SCRIPT>
			<script src="http://web.utk.edu/~oadebali/jquery-1.11.0.js"></script>
			<script src="http://cdvist.utk.edu/js/CDvistA.js"></script>
			<script src="http://cdvist.utk.edu/js/d3/d3.min.js"></script>
			<script src="http://cdvist.utk.edu/js/svg-pan-zoom.js"></script>
			<script src="http://web.utk.edu/~oadebali/jquery-ui.js"></script>'''

def htmlprint_scriptForInputForm(refreshTime):
	return '<script> setTimeout("document.search_form.submit()",' + str(refreshTime) + '000);</script>'
	
def htmlprint_closeHeadOpenBody():
	return '''
	</head>
	 
	 <body link="black">
	
'''

def htmlprint_majorButtonsTable():
	return '''
			<table id="header" border="0">
				<script>document.write(codostmenu());</script>	
				'''

def htmlPrintJSarchitecturesFromJSON(theId):
	return "<script>$.ajax({url: '" + theId + ".json',async: false,dataType: 'json',success: drawallFullJson});</script>"

def htmlprint_downloadMenu(htmlID):
	text = '''		<td class="firstrow" align="right">
					<div class="dropdown">
						<a class="download" >Export</a>
						<div class="submenu">
							<ul class="root">
								<li><a href="''' + htmlID + '''.json">JSON</input></a></li>
								<li><a href="../codost_out/''' + htmlID +  '''.html" download>HTML</a></li>
								<li><a onClick= "exportPDF(buttons);">PDF</a></li>
								<li><a href="#" onClick=" window.print(); return false" STYLE="TEXT-DECORATION: NONE">PRINT</a></li>
							</ul>
						</div>
					</div>
				</td>
				<script>download_menu();</script>
		'''
	return text

def htmlprint_closeButtonsTable():
		return '</tr>'

def htmlprint_refreshAndStatusTable():
	text =  '''
			<tr style="height:15px;">
		

			<td align="center" width="700" valign="top" id="statusbar" class="statusbars" colspan="7"></td>
			<td class="secondrow">
				<form name="search_form" method="post" action="/scripts/CDvistA_cgi2.py?htmlID='''
	text += ID
	text += '''" enctype="multipart/form-data">
        	                <input  id="refreshButton" name="Submit" type="submit" value="Status Check" style="width:115px; height: 20px; valign:top, font-size:8px"/>

			</td>
			<td class="secondrow">
				<input  id="killButton" name="Submit" type="submit" value="Kill Jobs" style="width:115px; height: 20px; valign:top, font-size:8px"/>
				</form>
			</tr>'''
	return text
			
parameters = getParameters(ID) 
def htmlprint_closeHeaderTableOpenMainTable():
	return'''
		</table>
		<div id="mymethod" class="method"><script>drawParam("''' + parameters + '''");</script></div>
		<table id="results" border="0", style='table-layout:fixed'>
	'''

def htmlprint_statusRow(tablename,proteincount,status):
	text = '<tr><td><td>'
	text=text+'<td valign="center"> ' + str(tablename) + '</td><td class="statusbars"><script>drawSinlgeStatus("'+str(status)+'");</script></td>'
	return text

def htmlprint_domainArchitectureRow(canvaslength,features,tablename,proteincount):
	list=features.split(",")
	tag=list[0]
	webFriendlyTag = co.removesymbols(tag)
	features.replace(tag,webFriendlyTag)
	del list[0]
	jsonname = tablename[0:-5] + ".json"	
	text= '''<tr>'''

	if newton:
		text=text+'<td><a title="log file" href=CDvist_'+ID+'.'+str(proteincount)+'.log>o</a></td>'
		#text=text+'<td><a title="error file" href=CODOST_'+ID+'.e.'+str(proteincount)+'>e</a></td>'
		text=text+'<td></td>'
	else:
		text=text+'<td></td><td></td>'
					
					
	text=text+'''
	<td><a title="individual JSON file" href="JavaScript:newpopup('
	'''
	text=text+jsonname
	
	text=text+'''',536,954);">'''
	text=text+tag		
	text=text+'''</a></td>
	<td width="'''+str(canvaslength)+'''">	
	<script>drawall('''
	text=text+features
	text=text+''')</script>	
		</td>			
		</tr>
		'''
	return text




def htmlprint_JSforStatusGraph(status_dict):
	q = status_dict['queued']
	r = status_dict['running']
	c = status_dict['completed']
	k = status_dict['killed']
	text = '''<script type="text/javascript">
	var bar = document.getElementById('statusbar').innerHTML;'''

	text += 'drawStatusBar('+str(q)+','+str(r)+','+str(c)+','+str(k)+');</script>'
	
	return text



def htmlprint_closeMainTable():
	return '''
		<script>myreset();stableTopBar();</script>
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-51633352-1', 'utk.edu');
		  ga('send', 'pageview');

		</script>		
		</table>
		'''

def htmlprint_parameterLine():
	pfile=open(parameterfile,'r')
	text='<p style="color:grey">'+pfile.readlines()[0]+'</p>'
	return text
	
def htmlprint_end():
	return '''
	</body>
</html>
		'''
	





#TableCount = len(glob('../codost_out/' + ID + "*.json"))
#TotalProteinCount = len(seq_list)
#complete = ifComplete(ID)
#if TableCount == TotalProteinCount:
#	with open('note.txt','w') as f:
#		f.write(str(TableCount) + " - " + str(TotalProteinCount))
#	complete = 1
#else:
#	complete = 0

if empty:
	statuspage = 1
else:
	if complete:
		statuspage = 0
	else:
		statuspage = 1

#cprint(outputfile)
out=open(outputfile,'w')		
out.write(htmlprint_initialize())

if statuspage==1 and empty==0 and requestTaken==1:
	task_stat_Dict = getStatusDictionary()

#complete = 0
if complete != 1:
	if empty ==1:
		refreshTime = 20 + int(floor(timeDiff(ID)))

	else:
		refreshTime = 1 + int(floor(timeDiff(ID)))
	out.write(htmlprint_scriptForInputForm(refreshTime))

out.write(htmlprint_closeHeadOpenBody())
out.write(htmlprint_majorButtonsTable())

if complete==1 or kill==1:
	mergeJsonFiles(ID)
	out.write(htmlprint_downloadMenu(str(ID)))
	os.system("rm STATUS"+str(ID))
out.write(htmlprint_closeButtonsTable())

if complete == 0 and kill == 0:
#	out.write("complete: " + str(complete))
#	out.write("\nseqlen: " + str(len(seq_list)))
#	out.write("\ntable: " + str(len(glob('../codost_out/' + ID + "*.json"))))
	out.write(htmlprint_refreshAndStatusTable())



out.write(htmlprint_closeHeaderTableOpenMainTable())
	
	
proteincount=1

status_dict={}
status_dict['queued'] = status_dict['running'] = status_dict['completed'] = status_dict['killed'] = status_dict['unknown'] = 0 



for proteinheader in seq_list:
	header=co.removesymbols(proteinheader)[0:50]
	#filename=str(ID)+header+'.table'
	filename=str(ID)+header+'.json'
	fullpathOfTable = '../codost_out/' + filename
	#print filename
	tableExist = glob(fullpathOfTable)
	if empty:
		status = "queued"
		out.write(htmlprint_statusRow(header,proteincount,status))
	else:
		if tableExist != []:
			if newton:
				#features=table2features(fullpathOfTable)
				features=co.json2features(fullpathOfTable)
				canvaslength=len(seq_dic[proteinheader])+5
				#out.write(htmlprint_domainArchitectureRow(canvaslength,features,filename,proteincount))
				status = "completed"

		elif kill:
			status = "killed"
			out.write(htmlprint_statusRow(header,proteincount,status))
		elif requestTaken:
			status = getStatus(proteincount,task_stat_Dict)
			out.write(htmlprint_statusRow(header,proteincount,status))
		else:
			status = "queued"
			out.write(htmlprint_statusRow(header,proteincount,status))
	if not status in status_dict.keys():
		status = "unknown"
	status_dict[status] += 1

	proteincount=proteincount+1

if complete==1 or kill==1:
	out.write(htmlPrintJSarchitecturesFromJSON(ID))

out.write(htmlprint_closeMainTable())

if param:
	out.write(htmlprint_parameterLine())
if complete!=1 and kill!=1:
	out.write(htmlprint_JSforStatusGraph(status_dict))
out.write(htmlprint_end())
out.close()

timeend=datetime.datetime.now()

timepassed=timeend-timestart

#cprint("Time spent:")
#cprint(str(timepassed))



