import execjs
import os
import sys
import urllib
import codostlib as co
import json


def json2svgfile(jsonstring,ID,buttonstring):
	#json_in = json.loads(jsonstring)
	json_in = jsonstring
	print json_in
	inputstring = co.jsondata2features(json_in)
	tag = inputstring.split(",")[0].replace("'","")
	svgname = tag + ".svg"
	#pngname = tag + ".png"

	buttonstring = "11111"

	command = "drawall2(" + inputstring + ",1,'" + buttonstring + "')" 
	print command
	#'accession_NP_415222.1_locus_b0694_genome_Escherich','MTNVLIVEDEQAIRRFLRTALEGDGMRVFEAETLQRGLLEAATRKPDLIILDLGLPDGDGIEFIRDLRQWSAVPVIVLSARSEESDKIAALDAGADDYLSKPFGIGELQARLRVALRRHSATTAPDPLVKFSDVTVDLAARVIHRGEEEVHLTPIEFRLLAVLLNNAGKVLTQRQLLNQVWGPNAVEHSHYLRIYMGHLRQKLEQDPARPRHFITETGIGYRFML','0_0_o','Response_reg;Trans_reg_C;','4-112;148-223;','101.9;77.5;','Pfam27.0;Pfam27.0;','4-112_1-111_112;148-223_2-77_77;','VLIVEDEQAIRRFLRTALEGDGM-RVFEAETLQRGLLEAATRKPDLIILDLGLPDGDGIEFIRDLRQWS-AVPVIVLSARSEESDKIAALDAGADDYLSKPFGIGELQARL;EEVHLTPIEFRLLAVLLNNAGKVLTQRQLLNQVWGPNAVEHSHYLRIYMGHLRQKLEQDPARPRHFITETGIGYRF;','vliv+De+++r+_lr+ale_+g+_+v_ea++_+__l__+_+++pDli+lD+_+p+_dG+e+++_+r+_+__+p+iv++a++ee+d_++al+aGa+d+lsKpf_+_el_+++;eev+ltp_ef+lLa+L++nag+v+++_+Ll++vwg_+____++_++_++_+LR+kle++p+_p++__t__g_GYr+;',1,"11011")"""


	url = "http://web.utk.edu/~oadebali/architecturesvg.js"


	env = urllib.urlopen(url).read().decode("utf-8")


	ctx = execjs.compile(env)
	out = open("../tmp/" + ID + "/" + svgname, "w") 
	out.write(ctx.eval(command))
	out.close()



def tar(ID,mode):
	code = "tar cvf ../codost_out/" + ID + "." + mode + ".tar ../tmp/" + ID +"/*." + mode
	os.system(code)

def mainloop(ID,buttonstring):
	os.system("mkdir ../tmp/" + ID)
	whole = json.loads(open("../codost_out/" + ID + ".json","r").read())
	for ind in whole:
		json2svgfile(ind,ID,buttonstring)

	tar(ID,"svg")


ID = "20140909201443276091"
buttonstring = "11111"

mainloop(ID,buttonstring)
