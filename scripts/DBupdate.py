import sys
import os
from ftplib import FTP


def getDirectoryNames(myftp):
        mylist = []
        newlist = []
        myftp.retrlines('LIST',mylist.append)
        for dir in mylist:
                #dir = dir.split(' ')[-1].strip().replace('\n','').replace('\/','')
                newlist.append(dir)
        return newlist


codost_root = "/var/www/cdvist.utk.edu/htdocs/codost/"
latestFile = codost_root + "DB_latest.txt"
currentFile = codost_root + "DB_current.txt"
diffFile = codost_root + "DB_diff.txt"

latest = open(latestFile,"r")
latestList = []
for line in latest:
	latestList.append(line)

out = open(currentFile,"w")
diff = open(diffFile,"w")

def writeDBlist(root,directory):

	ftp =  FTP(root)
	ftp.login("anonymous","ogunedebali@gmail.com")
	ftp.cwd(directory)
	DBs = getDirectoryNames(ftp)
	for db in DBs:
		text = db + "\t" + root + "/" + directory + "\n"
		out.write(text)
		if not text in latestList:
			diff.write(text)


writeDBlist('toolkit.genzentrum.lmu.de','pub/HH-suite/databases/hhsearch_dbs')
writeDBlist('toolkit.genzentrum.lmu.de','pub/HH-suite/databases/hhsuite_dbs')
writeDBlist('ftp.ebi.ac.uk','pub/databases/Pfam/releases')
writeDBlist('ftp.ncbi.nih.gov','pub/mmdb/cdd')

out.close()
diff.close()

os.system("cp " + currentFile + " " + latestFile)
os.system("mail -s 'CDvist Database Update Notice' oadebali@gmail.com <" + diffFile)
