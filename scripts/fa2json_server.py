import os
import sys
from codostlib import fastareader
import json


mydict = {}
d,l = fastareader(sys.argv[1])
out = open(sys.argv[2],"w")

mydict["entries"] = []

i = 0
for e in l:
	i += 1
	order = i
	name = e
	s = d[e]
	mydict["entries"].append({'header':name,'num':i,'fullsequence':s, "status":"queued"})

myjson = json.dumps(mydict, sort_keys=True, indent= 1)
out.write(myjson)
