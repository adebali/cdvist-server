import os
import sys
from codostlibJ import fastareader
import json


mydict = {}
ID = sys.argv[1]
dir = "/home/oadebali/codost/requests/" + ID + "/"
d,l = fastareader(dir + "FASTA" + ID + ".fa")
PARAM = open(dir + "PARAM" + ID + ".txt","r").readline()
out = open(dir + ID + ".json","w")


mydict["method"] = PARAM
mydict["entries"] = []

i = 0
for e in l:
	i += 1
	order = i
	name = e
	s = d[e]
	mydict["entries"].append({'header':name,'num':i,'fullsequence':s, "status":"queued"})
	mydict["param"] = PARAM

myjson = json.dumps(mydict, sort_keys=True, indent= 1)
out.write(myjson)
