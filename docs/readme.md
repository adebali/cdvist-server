CDvist
======


Domain Search Pipeline
----------------------

Pipeline explanation. 

###HMMER3 against Pfam


###RPSBLAST


###HHsearch against a database of interest

####HHsearch Databases
 * Pfam
 * PDB
 * CDD
 * COG
 * SCOP
 * SMART
 * TIGRFAM
 * PIRSF

####HHblits databases
 * NR
 * Uniprot

Other computations
------------------

###Transmembrane and Signal Peptide Predictions

###Coiled coils

###Low-complexity regions

Visualization
-------------

###Domain hits

###Coverage

###Alignment

###Sequence

###Topology


